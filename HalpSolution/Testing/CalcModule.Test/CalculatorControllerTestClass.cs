﻿// <copyright file="CalculatorControllerTestClass.cs" company="SoftServe">
// Copyright (c) 2013 All Rights Reserved
// </copyright>
// <author>Bilokin Sviatoslav</author>
// <date>3/6/2013 11:47:24 PM</date>
// <summary>TODO: Update summary</summary>
namespace CalcModule.Test
{
    using System;
    using System.Collections.Generic;
    using System.IO;
    using System.Text;
    using NUnit.Framework;
    using SoftServe.Halp.Modules.CalcModule;
    /// <summary>
    /// TODO: Update summary
    /// </summary>
    [TestFixture]
    public class CalculatorControllerTestClass
    {
        public IEnumerable<TestCaseData> getTransform
        {
            get
            {
                yield return new TestCaseData("HalpFormatData.csv", "HalpFormatData_transformed.csv");
                yield return new TestCaseData("Halpformat.csv", "Halpformat_transformed.csv");
            }
        }

        public IEnumerable<TestCaseData> getFormat
        {
            get
            {
                yield return new TestCaseData("HalpFormatData.csv", "HalpFormatData_formatted.xml", "xml");
                yield return new TestCaseData("HalpFormatData.csv", "HalpFormatData_formatted.bin", "bin");
                yield return new TestCaseData("HalpFormatData.csv", "HalpFormatData_formatted.json", "json");
            }
        }

        [TestCaseSource("getTransform")]
        public void Transform_ActionTest(string inputfile, string outputfile)
        {
            using (var controller = LogicContainer.Instance.GetService<SoftServe.Halp.Modules.CalcModule.Abstraction.ICalculatorContract>())
            {
                string directory = Path.Combine("Data", "ReadFormat");
                string output = Path.Combine(directory, outputfile);
                var code = controller.Transform(Path.Combine(directory, inputfile), output);
                Assert.IsTrue(File.Exists(output) || code == -21312);
            }
        }

        [TestCaseSource("getFormat")]
        public void Format_ActionTest(string inputfile, string outputfile, string format)
        {
            using (var controller = LogicContainer.Instance.GetService<SoftServe.Halp.Modules.CalcModule.Abstraction.ICalculatorContract>())
            {
                string directory = Path.Combine("Data", "ReadFormat");
                string input = Path.Combine(directory, outputfile);
                var code = controller.Format(Path.Combine(directory, inputfile),"csv", input, format);
                Assert.IsTrue(File.Exists(input) || code == -21312);
            }
        }
    }
}
