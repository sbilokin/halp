﻿

namespace SoftServe.Halp.Testing.CalcModule.Test.Comparers
{
    using System;
    using System.Collections.Generic;
    using SoftServe.Halp.Modules.CalcModule.Models;

    /// <summary>
    /// Provides comparasion logic of 2 HalpFormatData-instances 
    /// </summary>
    internal class HalpModelComparer : IComparer<HalpFormatData>
    {
        public int Compare(HalpFormatData element1, HalpFormatData element2)
        {
            if (element1 == null | element2 == null)
                return -1;

            if (element1.Id != element2.Id)
                return -1;

            if (string.Compare(element1.Name, element2.Name) != 0)
                return -1;

            var bRet = element2.V1.Equals(element2.V1) | element2.V2.Equals(element2.V2) | element2.V3.Equals(element2.V3);
            if (!bRet)
                return -1;

            return 0;
        }
    }
}
