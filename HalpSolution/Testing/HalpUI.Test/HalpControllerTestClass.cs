﻿// <copyright file="HalpControllerTestClass.cs" company="SoftServe">
// Copyright (c) 2013 All Rights Reserved
// </copyright>
// <author>Bilokin Sviatoslav</author>
// <date>3/6/2013 4:57:01 PM</date>
// <summary>TODO: Update summary</summary>
namespace CalcModule.Test
{
    using System;
    using System.Collections.Generic;
    using System.Globalization;
    using System.Text;
    using NUnit.Framework;
    using SoftServe.Halp.HalpUI;
    /// <summary>
    /// TODO: Update summary
    /// </summary>
    [TestFixture]
    public class HalpControllerTestClass
    {
        [Test]
        public void Localization_ActionTest()
        {
            using (var controller = LogicContainer.Instance.GetService<SoftServe.Halp.HalpUI.Abstraction.IHalpContract>())
            {
                controller.Initialize(new CultureInfo("en-US"));
                var sample = controller.ShowVer();
                Assert.IsTrue(sample.Contains("All rights reserved"));
            }
            using (var controller = LogicContainer.Instance.GetService<SoftServe.Halp.HalpUI.Abstraction.IHalpContract>())
            {
                controller.Initialize(new CultureInfo("ru-RU"));
                var sample = controller.ShowVer();
                Assert.IsTrue(sample.Contains("Все права защищены"));
            }
        }
    }
}
