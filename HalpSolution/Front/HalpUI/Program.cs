﻿// <copyright file="Program.cs" company="SoftServe">
// Copyright (c) 2013 All Rights Reserved
// </copyright>
// <author>Bilokin Sviatoslav</author>
// <date>3/2/2013 5:35:43 PM</date>
// <summary>TODO: Update summary</summary>
namespace HalpUI
{
    using System;
    using System.Collections.Generic;
    using System.Text;
    using System.Threading;
    using SoftServe.Halp.HalpUI.Controllers;
    /// <summary>
    /// Executeble class for Halp.
    /// </summary>
    class Program
    {
        /// <summary>
        /// Program's entry point
        /// </summary>
        /// <param name="args"></param>
        static void Main(string[] args)
        {
            using (var controller = new HalpController())
            {
                controller.Initialize(Thread.CurrentThread.CurrentUICulture);
                Console.WriteLine(controller.ShowHelloMessage());
                while (!controller.IsClosed)
                {
                    var command = controller.GetCommand();
                    if (string.Compare(command.Name, "invalid command", true) == 0)
                    {
                        Console.WriteLine(command.Name);
                        Console.WriteLine(controller.ShowHelp());
                        continue;
                    }
                    if (string.Compare(command.Name, "exit", true) == 0)
                    {
                        controller.IsClosed = true;
                        continue;
                    }
                    if (string.Compare(command.Name, "ver", true) == 0)
                    {
                        Console.WriteLine(controller.ShowVer());
                        continue;
                    }
                    if (string.Compare(command.Name, "help", true) == 0)
                    {
                        Console.WriteLine(controller.ShowHelp());
                        continue;
                    }
                    if (string.Compare(command.Name, "compute", true) == 0)
                    {
                        controller.Format(command.Parameters[0], command.Parameters[1], command.Parameters[2], command.Parameters[3]);
                        continue;
                    }
                    if (string.Compare(command.Name, "transform", true) == 0)
                    {
                        controller.Transform(command.Parameters[0], command.Parameters[1]);
                        continue;
                    }
                }
            }
        }
    }
}
