﻿// <copyright file="HalpController.cs" company="SoftServe">
// Copyright (c) 2013 All Rights Reserved
// </copyright>
// <author>Bilokin Sviatoslav</author>
// <date>3/2/2013 6:04:11 PM</date>
// <summary>TODO: Update summary</summary>
namespace SoftServe.Halp.HalpUI.Controllers
{
    using System;
    using System.Collections.Generic;
    using System.Globalization;
    using System.Resources;
    using System.Text;
    using Abstraction;
    using Models;
    /// <summary>
    /// Controller for interactive mode.
    /// </summary>
    internal class HalpController : IHalpContract
    {
        #region IHalpContract implementation
        public HalpController()
        {
            mCultures = new Dictionary<CultureInfo, Type>();
            mCultures.Add(new CultureInfo("en-US"), typeof(global::SoftServe.Halp.HalpUI.UIen));
            mCultures.Add(new CultureInfo("ru-RU"), typeof(global::SoftServe.Halp.HalpUI.UIru));
        }
        /// <summary>
        /// Initializes controller instance.
        /// </summary>
        /// <param name="culture">Culture for localization</param>
        public void Initialize(CultureInfo culture)
        {
            if (culture != default(CultureInfo) && mCultures.ContainsKey(culture))
            {
                mResourceManager = new ResourceManager(mCultures[culture]);
                return;
            }
            mResourceManager = new ResourceManager(typeof(global::SoftServe.Halp.HalpUI.UIen));
        }
        /// <summary>
        /// Returns startup message.
        /// </summary>
        /// <returns>Startup message</returns>
        public string ShowHelloMessage()
        {
            StringBuilder s = new StringBuilder(ShowVer());
            s.AppendLine();
            for (int i = 0; i < Console.WindowWidth; i++)
            {
                s.Append('=');
            }
            return s.ToString();
        }
        /// <summary>
        /// Shows message and promt string from user.
        /// </summary>
        /// <returns>Parsed command</returns>
        public Command GetCommand()
        {
            var parser = new CommandParser();
            Console.WriteLine(mResourceManager.GetString("Promt"));
            string input = Console.ReadLine();
            return parser.GetCommand(input);
        }
        /// <summary>
        /// Shows help string.
        /// </summary>
        /// <returns></returns>
        public string ShowHelp()
        {
            return mResourceManager.GetString("Help");
        }
        /// <summary>
        /// Returns version string.
        /// </summary>
        /// <returns></returns>
        public string ShowVer()
        {
            return mResourceManager.GetString("Version");
        }
        /// <summary>
        /// Indicator for state of controller.
        /// </summary>
        public bool IsClosed
        {
            get;
            set;
        }
        /// <summary>
        /// Invokes controller for fpTransform utility.
        /// </summary>
        /// <param name="inputFile">Input file path</param>
        /// <param name="outputFile"></param>
        public void Transform(string inputFile, string outputFile)
        {
            using (var controller = Modules.CalcModule.LogicContainer.Instance.GetService<Modules.CalcModule.Abstraction.ICalculatorContract>())
            {
                var success = controller.Transform(inputFile, outputFile);
                if (success == 0)
                {
                    Console.WriteLine(mResourceManager.GetString("Success"));
                }
                else
                {
                    Console.WriteLine(mResourceManager.GetString("Fail"));
                }
            }
        }
        /// <summary>
        /// Invokes controller for fpFormat utility.
        /// </summary>
        /// <param name="inputFile">Input file path</param>
        /// <param name="inputFormat">Input file format</param>
        /// <param name="outputFile">Output file path</param>
        /// <param name="outputFormat">Output file format</param>
        public void Format(string inputFile, string inputFormat, string outputFile, string outputFormat)
        {
            using (var controller = Modules.CalcModule.LogicContainer.Instance.GetService<Modules.CalcModule.Abstraction.ICalculatorContract>())
            {
                var success = controller.Format(inputFile, inputFormat, outputFile, outputFormat);
                if (success == 0)
                {
                    Console.WriteLine("Operation successful!");
                }
                else
                {
                    Console.WriteLine("Error!");
                }
            }
        }
        #endregion

        #region IDisposable implementation

        public void Dispose()
        {
            dispose(true);
            GC.SuppressFinalize(this);
        }

        private void dispose(bool disposeManaged)
        {
            if (mDisposed)
                return;
            mDisposed = true;
        }
        #endregion

        private readonly Dictionary<CultureInfo, Type> mCultures = default(Dictionary<CultureInfo, Type>);
        private ResourceManager mResourceManager = default(ResourceManager);
        private bool mDisposed = false;
    }
}
