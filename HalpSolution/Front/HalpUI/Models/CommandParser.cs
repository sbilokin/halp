﻿// <copyright file="CommandParser.cs" company="SoftServe">
// Copyright (c) 2013 All Rights Reserved
// </copyright>
// <author>Bilokin Sviatoslav</author>
// <date>2/22/2013 10:02:15 PM</date>
// <summary>TODO: Update summary</summary>
namespace SoftServe.Halp.HalpUI.Models
{
    using System;
    using System.Collections.Generic;
    using System.Text;
    using System.Text.RegularExpressions;
    /// <summary>
    /// Class for command parsing.
    /// </summary>
    internal class CommandParser
    {
        static CommandParser()
        {
            mRegexCommandsNames = new List<string>();
            mRegexCommandsNames.Add(@"^(help)$");
            mRegexCommandsNames.Add(@"^(exit)$");
            //mRegexCommandsNames.Add(@"^(clear)$");
            mRegexCommandsNames.Add(@"^(ver)$");
            mRegexCommandsNames.Add(@"^(create)\s([^\s]+)$");
            mRegexCommandsNames.Add(@"^(transform)\s([^\s]+)\s([^\s]+)$");
            mRegexCommandsNames.Add(@"^(compute)\s([^\s]+)\s([^\s]+)\s([^\s]+)\s([^\s]+)$");
            //mRegexCommandsNames.Add(@"^(addtriangle)\s(\d+)$");
            //mRegexCommandsNames.Add(@"^(write)\s([^\s]+)\s([^\s]+)$");
        }
        /// <summary>
        /// Returns Command parsed using regex.
        /// </summary>
        /// <param name="input">Input string</param>
        /// <returns>Command</returns>
        public Command GetCommand(string input)
        {
            Command command = new Command();
            foreach (var key in mRegexCommandsNames)
            {
                Match match = new Regex(key).Match(input);
                if (match.Success)
                {
                    var groups = match.Groups;
                    command.Name = groups[1].ToString();
                    if (groups.Count > 1)
                    {
                        for (int i = 2; i < groups.Count; i++)
                        {
                            command.Parameters.Add(groups[i].ToString());
                        }
                    }
                }
            }
            if (string.IsNullOrEmpty(command.Name))
            {
                command.Name = "invalid command";
            }
            return command;
        }
        /// <summary>
        ///  List that contains regex strings for each command.
        /// </summary>
        public static List<string> mRegexCommandsNames { get; set; }
    }
}
