﻿// <copyright file="CommandBase.cs" company="SoftServe">
// Copyright (c) 2013 All Rights Reserved
// </copyright>
// <author>Bilokin Sviatoslav</author>
// <date>2/22/2013 9:32:57 PM</date>
// <summary>TODO: Update summary</summary>
namespace SoftServe.Halp.HalpUI.Models
{
    using System;
    using System.Collections.Generic;
    using System.Text;
    /// <summary>
    /// Class for command realization.
    /// </summary>
    public sealed class Command
    {
        /// <summary>
        /// Returns empty command.
        /// </summary>
        public Command()
        {
            Parameters = new List<string>();
        }
        /// <summary>
        /// Returns command with name only.
        /// </summary>
        /// <param name="name"></param>
        public Command(string name)
            : this()
        {
            Name = name;
        }
        /// <summary>
        /// Name.
        /// </summary>
        public string Name { get; set; }
        /// <summary>
        /// Parameters of command.
        /// </summary>
        public List<string> Parameters { get; set; }

        public override string ToString()
        {
            if (Parameters != null)
            {
                StringBuilder result = new StringBuilder(Name);
                foreach (var item in Parameters)
                {
                    result.Append(' ');
                    result.Append(item);
                }
                return result.ToString();

            }
            return Name;
        }
    }
}
