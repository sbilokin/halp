﻿// <copyright file="ICommandParser.cs" company="SoftServe">
// Copyright (c) 2013 All Rights Reserved
// </copyright>
// <author>Bilokin Sviatoslav</author>
// <date>2/22/2013 10:02:28 PM</date>
// <summary>TODO: Update summary</summary>
namespace SoftServe.Halp.HalpUI.Abstraction
{
    using System;
    using System.Collections.Generic;
    using System.Text;
    using Models;
    /// <summary>
    /// Interface for parser
    /// </summary>
    internal interface ICommandParser : IDisposable
    {
        /// <summary>
        /// Returns a filled Command instance, according to input string.
        /// </summary>
        /// <param name="input"></param>
        /// <returns></returns>
        Command Parse(string input);
    }
}
