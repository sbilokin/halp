﻿// <copyright file="IHalpContract.cs" company="SoftServe">
// Copyright (c) 2013 All Rights Reserved
// </copyright>
// <author>Bilokin Sviatoslav</author>
// <date>3/2/2013 6:03:52 PM</date>
// <summary>TODO: Update summary</summary>
namespace SoftServe.Halp.HalpUI.Abstraction
{
    using System;
using System.Collections.Generic;
using System.Globalization;
using System.Text;
using Models;
    /// <summary>
    /// Contract for interactive mode.
    /// </summary>
    public interface IHalpContract : IDisposable
    {
        /// <summary>
        /// Initializes controller instance.
        /// </summary>
        /// <param name="culture">Culture for localization</param>
        void Initialize(CultureInfo culture);
        /// <summary>
        /// Returns startup message.
        /// </summary>
        /// <returns>Startup message</returns>
        string ShowHelloMessage();
        /// <summary>
        /// Shows message and promt string from user.
        /// </summary>
        /// <returns>Parsed command</returns>
        Command GetCommand();
        /// <summary>
        /// Shows help string.
        /// </summary>
        /// <returns></returns>
        string ShowHelp();
        /// <summary>
        /// Returns version string.
        /// </summary>
        /// <returns></returns>
        string ShowVer();
        /// <summary>
        /// Indicator for state of controller.
        /// </summary>
        bool IsClosed { get; set; }
        /// <summary>
        /// Executes fpTransform utility.
        /// </summary>
        /// <param name="inputFile">Input file path</param>
        /// <param name="outputFile"></param>
        void Transform(string inputFile, string outputFile);
        /// <summary>
        /// Executes fpFormat utility.
        /// </summary>
        /// <param name="inputFile">Input file path</param>
        /// <param name="inputFormat">Input file format</param>
        /// <param name="outputFile">Output file path</param>
        /// <param name="outputFormat">Output file format</param>
        void Format(string inputFile, string inputFormat, string outputFile, string outputFormat);
    }
}
