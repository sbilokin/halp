﻿// -----------------------------------------------------------------------
// <copyright file="TransformMoq.cs" company="">
// TODO: Update copyright text.
// </copyright>
// -----------------------------------------------------------------------

namespace SoftServe.Halp.Modules.CalcModule.Proxies
{
    using System;
    using System.IO;   
    
    using SoftServe.Halp.Modules.CalcModule.Abstraction;

    using Moq;
    using SoftServe.Halp.Modules.CalcModule.Models;


    /// <summary>
    /// TODO: Update summary.
    /// </summary>
    public static class Moqs
    {
        static Moqs()
        {
            try
            {
                Initalize();
            }
            catch (Exception)
            {
                /// TODO: Add log audit logic here
            }
        }

        public static ITransformContract TransformProxyMoq { get { return mTransformProxyMoq.Object; } }
        public static ICalculatorContract CalcProxyMoq { get { return mCalcProxyMoq.Object; } }
        public static IFormatContract FormatProxyMoq { get { return mFormatProxyMoq.Object; } }

        private static void Initalize()
        {
            lock (__mutex)
            {
                initializeFormat();
                initializeTransform();
                initializeCalc();
            }
        }

        private static void initializeCalc()
        {
            
        }

        private static void initializeTransform()
        {
            var halp = new HalpFormatData();
            mTransformProxyMoq.Setup(x => x.ComputeTriangle(new HalpFormatData(), out halp));
        }

        private static void initializeFormat()
        {
            string input = "inputFormat.csv";
            mFormatProxyMoq.Setup(x => x.DetectFormatTypeAtExt(input)).Returns(() => (File.Exists(input) ? eFormatType.XML : eFormatType.Undefined));
        }

        private static readonly object __mutex = new object();
        private static readonly Mock<IFormatContract> mFormatProxyMoq = new Mock<IFormatContract>(MockBehavior.Loose);
        private static readonly Mock<ITransformContract> mTransformProxyMoq = new Mock<ITransformContract>(MockBehavior.Loose);
        private static readonly Mock<ICalculatorContract> mCalcProxyMoq = new Mock<ICalculatorContract>(MockBehavior.Loose);
    }
}
