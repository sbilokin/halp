﻿// -----------------------------------------------------------------------
// <copyright file="ICalculatorContract.cs" company="">
// TODO: Update copyright text.
// </copyright>
// -----------------------------------------------------------------------

namespace SoftServe.Halp.Modules.CalcModule.Abstraction
{
    using System;
    using System.Collections.Generic;
    using System.Linq;
    using System.Text;

    /// <summary>
    /// Contract for Calculator controller.
    /// </summary>
    public interface ICalculatorContract :IDisposable
    {
        /// <summary>
        /// Transform data from input file and writes to output file.
        /// </summary>
        /// <param name="input">Path for input file</param>
        /// <param name="output">Path for output file</param>
        /// <returns>Output code</returns>
        int Transform(string input, string output);
        /// <summary>
        /// Execute formatting input file to output with specified format.
        /// </summary>
        /// <param name="input">Path for input file</param>
        /// <param name="output">Path for output file</param>
        /// <param name="formatOfOutput">Specified format</param>
        /// <returns>Output code</returns>
        int Format(string input, string formatOfInput, string output, string formatOfOutput);
    }
}
