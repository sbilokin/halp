﻿// -----------------------------------------------------------------------
// <copyright file="CalculatorController.cs" company="">
// TODO: Update copyright text.
// </copyright>
// -----------------------------------------------------------------------

namespace SoftServe.Halp.Modules.CalcModule.Controllers
{
    using System;
    using System.Diagnostics;
    using System.IO;

    /// <summary>
    /// TODO: Update summary.
    /// </summary>
    internal class CalculatorController : Abstraction.ICalculatorContract
    {
        #region ICalculatorContract implementation
        /// <summary>
        /// Run transform utility.
        /// </summary>
        /// <param name="input"></param>
        /// <param name="output"></param>
        /// <returns></returns>
        public int Transform(string input, string output)
        {
            string template = "{0} {1}";
            return Run(Path.Combine("Utilities", "fpTransform.exe"), string.Format(template, input, output));
        }
        /// <summary>
        /// Run format utility.
        /// </summary>
        /// <param name="input"></param>
        /// <param name="formatOfInput"></param>
        /// <param name="output"></param>
        /// <param name="formatOfOutput"></param>
        /// <returns></returns>
        public int Format(string input, string formatOfInput, string output, string formatOfOutput)
        {
            string args = "in:{0} from:{1} out:{2} to:{3}";
            return Run(Path.Combine("Utilities", "fpFormat.exe"), string.Format(args, input, formatOfInput, output, formatOfOutput));
        }
        #endregion
        /// <summary>
        /// Run process.
        /// </summary>
        /// <param name="fileName">Path to exe</param>
        /// <param name="args">Arguments</param>
        /// <returns>Exit code</returns>
        private static int Run(string fileName, string args)
        {
            if (!File.Exists(fileName))
            {
                return -1;
            }
            string returnvalue = string.Empty;

            var info = new ProcessStartInfo(fileName)
            {
                UseShellExecute = false,
                Arguments = args,
                RedirectStandardInput = true,
                RedirectStandardOutput = true,
                CreateNoWindow = true
            };

            using (var process = Process.Start(info))
            {
                using (var reader = process.StandardOutput)
                {
                    string input = reader.ReadToEnd();
                }
                return process.ExitCode;
            }
        }

        #region IDisposable implementation

        public void Dispose()
        {
            dispose(true);
            GC.SuppressFinalize(this);
        }

        private void dispose(bool disposeManaged)
        {
            if (mDisposed)
                return;
            mDisposed = true;
        }
        #endregion

        private bool mDisposed = false;
    }
}
